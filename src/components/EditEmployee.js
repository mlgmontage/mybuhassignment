import React from "react";

const EditEmployee = ({
  id,
  formData,
  setFormdata,
  handleCreateSubmit,
  handleEditSubmit,
  create,
}) => {
  return (
    <tr>
      {/* Name */}
      <td>
        <input
          type="text"
          className="uk-input"
          onChange={(e) => setFormdata.setName(e.target.value)}
          value={formData.name}
        />
      </td>

      {/* Position */}
      <td>
        <input
          type="text"
          className="uk-input"
          onChange={(e) => setFormdata.setPosition(e.target.value)}
          value={formData.position}
        />
      </td>

      {/* Social status */}
      <td>
        <div className="pretty p-switch">
          <input
            type="checkbox"
            onChange={(e) =>
              setFormdata.setSocial({
                ...formData.social,
                resident: e.target.checked,
              })
            }
            checked={formData.social.resident}
          />
          <div className="state p-primary">
            <label>Резидент</label>
          </div>
        </div>
        <br />
        <div className="pretty p-switch">
          <input
            type="checkbox"
            onChange={(e) =>
              setFormdata.setSocial({
                ...formData.social,
                pensioner: e.target.checked,
              })
            }
            checked={formData.social.pensioner}
          />
          <div className="state p-primary">
            <label>Пенсионер</label>
          </div>
        </div>
        <br />
        <div className="pretty p-switch">
          <input
            type="checkbox"
            onChange={(e) =>
              setFormdata.setSocial({
                ...formData.social,
                disabled: e.target.checked,
              })
            }
            checked={formData.social.disabled}
          />
          <div className="state p-primary">
            <label>Инвалид</label>
          </div>
        </div>
      </td>

      {/* Salary */}
      <td>
        {formData.salaries.map((salary, ind) => (
          <div className="uk-margin-bottom" key={ind}>
            <input
              className="uk-input"
              type="text"
              value={salary.sum}
              onChange={(e) => {
                const list = [...formData.salaries];
                list[ind].sum = e.target.value;
                setFormdata.setSalaries(list);
              }}
            />
            <input
              className="uk-input"
              type="text"
              value={salary.date}
              onChange={(e) => {
                const list = [...formData.salaries];
                list[ind].date = e.target.value;
                setFormdata.setSalaries(list);
              }}
            />
            <div
              className="uk-text-danger underlined"
              onClick={() => {
                let list = [...formData.salaries];
                list = list.filter((val, i) => ind !== i);
                setFormdata.setSalaries(list);
              }}
            >
              Удалить
            </div>
          </div>
        ))}
        <div
          className="uk-text-primary underlined"
          onClick={() => {
            const salary = { sum: "", date: "" };
            const list = [...formData.salaries, salary];
            setFormdata.setSalaries(list);
          }}
        >
          Добавить оклад
        </div>
      </td>

      {/* Actions */}
      <td>
        {(create && (
          <div
            onClick={() => handleCreateSubmit()}
            className="uk-text-primary underlined"
          >
            Создать
          </div>
        )) || (
          <div
            onClick={() => handleEditSubmit(id)}
            className="uk-text-primary underlined"
          >
            Сохранить
          </div>
        )}
      </td>
    </tr>
  );
};

export default EditEmployee;
