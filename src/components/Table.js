import React from "react";
import EditEmployee from "./EditEmployee";
import ShowEmployee from "./ShowEmployee";

const Table = ({
  employees,
  handleDelete,
  handleCopy,
  handleEdit,
  formData,
  setFormdata,
  create,
  handleEditSubmit,
  handleCreateSubmit,
}) => {
  return (
    <table className="uk-table">
      <thead className="uk-background-muted">
        <tr>
          <th>ФИО</th>
          <th>Должность</th>
          <th>Социалные статусы</th>
          <th>Оклады</th>
          <th>Действия</th>
        </tr>
      </thead>

      <tbody>
        {create && (
          <EditEmployee
            formData={formData}
            setFormdata={setFormdata}
            handleCreateSubmit={handleCreateSubmit}
            handleEditSubmit={handleEditSubmit}
            create={create}
          />
        )}
        {employees.map(
          (entry, id) =>
            (!entry.editable && (
              <ShowEmployee
                key={entry.name}
                entry={entry}
                id={id}
                handleDelete={handleDelete}
                handleCopy={handleCopy}
                handleEdit={handleEdit}
              />
            )) ||
            (entry.editable && (
              <EditEmployee
                id={id}
                key={entry.name}
                formData={formData}
                setFormdata={setFormdata}
                handleCreateSubmit={handleCreateSubmit}
                handleEditSubmit={handleEditSubmit}
              />
            ))
        )}
      </tbody>
    </table>
  );
};

export default Table;
