import React from "react";

const ShowEmployee = ({ entry, id, handleDelete, handleCopy, handleEdit }) => (
  <tr key={entry.name}>
    {/* Name */}
    <td>{entry.name}</td>

    {/* Position */}
    <td>{entry.position}</td>

    {/* Social status */}
    <td>
      <div className="pretty p-switch">
        <input readOnly type="checkbox" checked={entry.social.resident} />
        <div className="state p-primary">
          <label>Резидент</label>
        </div>
      </div>
      <br />
      <div className="pretty p-switch">
        <input readOnly type="checkbox" checked={entry.social.pensioner} />
        <div className="state p-primary">
          <label>Пенсионер</label>
        </div>
      </div>
      <br />
      <div className="pretty p-switch">
        <input readOnly type="checkbox" checked={entry.social.disabled} />
        <div className="state p-primary">
          <label>Инвалид</label>
        </div>
      </div>
    </td>

    {/* Salary */}
    <td>
      {entry.salaries.map((salary) => (
        <div
          key={salary.date}
          className="uk-margin-bottom"
          style={{ borderBottom: "1px solid #e5e5e5" }}
        >
          <div>{salary.sum}</div>
          <div className="uk-text-small uk-text-muted">{salary.date}</div>
        </div>
      ))}
    </td>

    {/* Actions */}
    <td>
      <div
        onClick={() => handleCopy(id)}
        className="uk-text-primary underlined"
      >
        Скопировать
      </div>
      <br />
      <div
        onClick={() => handleEdit(id)}
        className="uk-text-primary underlined"
      >
        Изменить
      </div>
      <br />
      <div
        onClick={() => handleDelete(id)}
        className="uk-text-danger underlined"
      >
        Удалить
      </div>
    </td>
  </tr>
);

export default ShowEmployee;
