import React, { useState, useEffect } from "react";
import { getEmployeesAPI } from "./data/employees";
import Table from "./components/Table";

function App() {
  const [data, setData] = useState([]);

  const [name, setName] = useState("");
  const [position, setPosition] = useState("");
  const [social, setSocial] = useState({
    resident: false,
    pensioner: false,
    disabled: false,
  });
  const [salaries, setSalaries] = useState([]);
  const [create, setCreate] = useState(false);

  useEffect(async () => {
    const response = await getEmployeesAPI();
    setData(response);
    console.log("fetched");
  }, []);

  // handle cancel
  const handleCancel = () => {
    console.log("cancel");
    const list = [...data];
    list.map((item) => (item.editable = false));
    setCreate(false);
    setData(list);
  };

  // handle create
  const handleCreate = () => {
    setCreate(true);
  };

  // reset form
  const reset = () => {
    setName("");
    setPosition("");
    setSocial({
      resident: false,
      pensioner: false,
      disabled: false,
    });
    setSalaries([]);
  };

  // code for handling edit
  const handleEdit = (id) => {
    console.log("edit", id);
    const list = [...data];

    // only one editable
    list.map((item) => (item.editable = false));
    list[id].editable = true;

    // load to form
    const editing = list[id];
    setName(editing.name);
    setPosition(editing.position);
    setSocial(editing.social);
    setSalaries(editing.salaries);
    setData(list);
  };

  // === methods with API calls ===

  // handle Edit submit
  const handleEditSubmit = (id) => {
    const list = [...data];
    list[id].name = name;
    list[id].position = position;
    list[id].social = social;
    list[id].salaries = salaries;
    list.map((item) => (item.editable = false));
    setData(list);
    reset();
    console.log("API call to update/save", {
      name,
      position,
      social,
      salaries,
    });
  };

  // handle Create submit
  const handleCreateSubmit = () => {
    const employee = {
      name,
      position,
      social,
      salaries,
    };
    const list = [employee, ...data];
    setData(list);
    setCreate(false);

    reset();
    console.log("API call to create", employee);
  };

  // code for handling copy
  const handleCopy = (id) => {
    console.log("API call to copy", id);
    const list = [...data];
    list.splice(id, 0, data[id]);
    setData(list);
  };

  // code for handling delete
  const handleDelete = (id) => {
    console.log("API call to delete", id);
    setData(data.filter((val, ind) => id != ind));
  };

  return (
    <div className="container">
      <div>
        <h1 style={{ display: "inline-block" }}>Работники</h1>
        <div style={{ float: "right" }}>
          <button
            onClick={handleCreate}
            className="uk-button uk-button-default"
          >
            <i uk-icon="icon: plus"></i>
            Добавить работника
          </button>
        </div>
      </div>
      <Table
        employees={data}
        handleDelete={handleDelete}
        handleCopy={handleCopy}
        handleEdit={handleEdit}
        formData={{ name, position, social, salaries }}
        setFormdata={{ setName, setPosition, setSocial, setSalaries }}
        create={create}
        handleCreateSubmit={handleCreateSubmit}
        handleEditSubmit={handleEditSubmit}
      />
      <button onClick={handleCancel} className="uk-button uk-button-default">
        Отменить
      </button>{" "}
      {/* This button is redundant, becuase save action is already on table's actions column */}
      <button className="uk-button uk-button-primary">Сохранить</button>
    </div>
  );
}

export default App;
